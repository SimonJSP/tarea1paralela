#include <iostream>
#include <stdio.h>
#include <fstream> // funcion para el trabajo con ficheros
#include <math.h> // libreria para utilizar la funcion sqrt(x)


using namespace std;

int main()
{
    cout <<"Bienvenido ! "<<endl;
    cout << "Cargando y calculando datos estadísticos..."<<endl;
    char palabra[10]; // VARIABLE PARA ALMACENAR CADA LINEA
    int i;   //CONTADOR
    double nota,suma,sumavar,promedio,desviacionest; //VARIABLES FLOTANTES PARA REALIZAR CALCULOS
    ifstream fichero("numeros.txt"); //ABRIENDO ARCHIVO
    if(fichero.bad()){
        cout <<"ERROR DE LECTURA DEL ARCHIVO"<<endl; //SI TIENE UN ERRROR DE LECTURA, ENVIA MENSAJE
    }
    else{
        while(fichero>>palabra){ //CONDICIÓN QUE RESUELVE LA DUPLICIDAD EN LA ULTIMA LÍNEA AL USAR LA FUNCION EOF()
            for(int j=0; j<=2; j++)
            if (palabra[j]== ',')
            {                   // REEMPLAZANDO COMA POR PUNTO
                palabra[j]='.';
            break;
            }
            nota = atof(palabra); // CONVIRTIENDO CADENA A STRING
            i++; // CONTADOR DE DATOS DEL ARCHIVO
            suma = suma + nota; //SUMATORIA DE NOTAS
        }
        promedio = suma/i; // CALCULO DE PROMEDIO
    }
    fichero.clear(); // LIMPIAR
    fichero.seekg(0,fichero.beg); // MOVER EL CURSOR AL PRINCIPIO DEL FICHERO
    while(fichero>>palabra){
            for(int j=0; j<=2; j++) // LAS LINEAS DEL ARCHIVO TIENEN 3 CARACTERES, POR ENDE EL CONTADOR LLEGA HASTA 2
            if (palabra[j]== ',')
            {
                palabra[j]='.';  // REEMPLAZANDO COMAS POR PUNTO
            break;
            }
            nota = atof(palabra);  // CONVIRTIENDO CADENA DE CARACTERES A FLOAT
            sumavar= sumavar + pow((nota-promedio),2); // SUMA PARA CALCULAR LA VARIANZA Y DESVIACIÓN
        }
        desviacionest = sqrt(sumavar/(i-1)); // CALCULO DE DESVIACION ESTANDAR
        cout<<"El número de elementos que contiene archivo es : " << i<<endl;
        cout<<"La suma de los elementos del archivo es : "<<suma<<endl;
        cout<<"El promedio de notas es: "<<promedio<<endl;
        cout<<"Desviación estandar : "<<desviacionest;
    fichero.close(); // CERRANDO LECTURA DE ARCHIVO
}
